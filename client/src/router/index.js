/*eslint-disable*/
import Vue from 'vue';
import VueRouter from 'vue-router';
import CreatePost from '../views/CreatePost.vue';
import RegisterUser from '../views/RegisterUser.vue';
import LoginUser from '../views/LoginUser.vue';
import LogoutUser from '../views/LogoutUser.vue';
import ListPosts from '../views/ListPosts.vue';
import ViewPost from '../views/ViewPost.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'ListPosts',
    component: ListPosts,
  },
  {
    path: '/createPost',
    name: 'CreatePost',
    component: CreatePost,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/register',
    name: 'RegisterUser',
    component: RegisterUser,
  },
  {
    path: '/login',
    name: 'LoginUser',
    component: LoginUser,
  },
  {
    path: '/logout',
    name: 'LogoutUser',
    component: LogoutUser,
  },
  { 
    path: '/viewPost/:postId', 
    name: 'ViewPost',
    component: ViewPost
  }
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('email') == null || localStorage.getItem('password') == null) {
        next({
          path: '/login',
          params: { nextUrl: to.fullPath }
        })
      } else {
        next()          
      }
  } else {
      next()
  }
})

export default router;
