const Post = require('../models/post');

exports.index = function (req, res) {
  Post.findById(req.params.postId).exec(function (err, post) {
    if (err) {
      console.log(err);
      return res.status(500).send("System error");
    }
    res.send(post);
  });
};

exports.create = async function (req, res) {
  const postData = req.body;
  postData.created = Date.now();
  const newPost = new Post(req.body);
  if (req.file && req.file.fieldname === 'thumbnail') {
    newPost.thumbnail = req.file.filename;
  }
  newPost.save(function (err, data) {
    if (err) {
      console.error(err);
      res.status(500).send('System error');
    } else {
      res.send(data);
    }
  });
};

exports.list = function (req, res) {
  Post.find({}).sort({created: 'desc'}).exec(function (err, posts) {
    if (err) {
      console.error(err);
      return res.status(500).send("System error");
    }
    res.send(posts);
  });
};
