const User = require('../models/user');
const bcrypt = require('bcrypt');

exports.register = async function (req, res) {
  newUser= new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  })
  await newUser.save().catch((e) => {
    console.error(e);
    res.status(400).send(e.message);
  })
  const matchedUsers = await User.find({email: req.body.email}).catch((e) => {
    console.error(e);
    res.status(500).send('System error');
  })
  if (!matchedUsers.length) {
    console.error("User couldn't be saved in database")
    res.status(500).send("System error");
  }
  res.status(200).send({ success:true });  
};

exports.login= async function (req, res) {
  let matchedUsers = [];
  try {
    matchedUsers = await User.find({email: req.body.email})
  } catch(e) {
    console.error(e);
    return res.status(500).send("System error");
  }
  if (!matchedUsers.length){ 
    return res.status(404).send("No user found for the provided email");
  }
  const user = matchedUsers[0];
  let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
  if (!passwordIsValid) return res.status(401).send("Invalid password");
  return res.status(200).send({ success:true });
}
