const express = require('express');
const cors = require('cors');
//Import required to initialize db connection
const db = require('./db.js')
const app = express();
const post = require('./routes/post');
const user = require('./routes/user');
const bodyParser = require('body-parser');

const path = __dirname + '/public/views';
const port = process.env.BLOG_PORT || 3000;
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }));
app.use(cors({credentials: true, origin: true, methods: ['GET', 'PUT', 'POST']}));
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use('/post', post);
app.use('/user', user);

app.listen(port, function () {
  console.log(`Example app listening on ${port}!`);
});
