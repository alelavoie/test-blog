const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Post = new Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  thumbnail: { type: String },
  created: { type: Date, required: true },
});

module.exports = mongoose.model('Post', Post);
