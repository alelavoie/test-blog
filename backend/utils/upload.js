const fs = require('fs');
const multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/upload');
  },
  filename: function (req, file, cb) {
    if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
      cb(new Error('File type not supported for thumbnail'));
    }
    const fileExtension = file.mimetype === 'image/jpeg' ? '.jpg' : '.png';
    cb(null, file.fieldname + '-' + Date.now() + fileExtension);
  },
});

const upload = multer({ storage: storage });

module.exports = {
  upload,
};
