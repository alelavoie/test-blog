const fs = require('fs');
const multer = require('multer');
const User = require('../models/user')
const bcrypt = require('bcrypt');

var validateUser = async function(username, password, cb) {
  const matchedUsers = await User.find({email: username})
  if (!matchedUsers.length) {
    return cb(null, false);
  }
  const user = matchedUsers[0];
  let passwordIsValid = bcrypt.compareSync(password, user.password);
  return cb(null, passwordIsValid)
}


module.exports = {
  validateUser,
};
