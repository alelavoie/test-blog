const express = require('express');
const router = express.Router();
const post = require('../controllers/post');
const upload = require('../utils/upload').upload;
const validateUser = require('../utils/auth').validateUser;
const basicAuth = require('express-basic-auth')

router.get('/view/:postId', function (req, res) {
  post.index(req, res);
});

router.post('/create', basicAuth({authorizer: validateUser, authorizeAsync: true, unauthorizedResponse: {error: 'No or wrong credentials provided'}}), upload.single('thumbnail'), function (req, res) {
  post.create(req, res);
});

router.get('/list', function (req, res) {
  post.list(req, res);
});

module.exports = router;
