
const user = require('../controllers/user');
const express = require('express');
const router = express.Router();

router.post('/register', function(req, res) {
    user.register(req, res)    
});

router.post('/login', function(req, res) {
    user.login(req, res)    
});

module.exports = router;