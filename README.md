# Blog test

## Installation
- `git clone https://alelavoie@bitbucket.org/alelavoie/test-blog.git`
- `cd test-blog`

## Running the app
- Make sure Docker Desktop is installed (Mac/Windows). Note: if you are on Windows, you are on your own and should seriously consider switching to Mac or Linux ;-)
- For linux, see the following instructions to install Docker Compose: https://docs.docker.com/compose/install/#install-compose-on-linux-systems
- `docker-compose up`
- Access `http://localhost` in the browser

## Other userful commands 

### To clear the volumes (WARNING - data will be lost)
- `docker-compose down -v`

### Rebuild the images (when you add new packages)
- `docker-compose build --no-cache`